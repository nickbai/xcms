<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/24
 * Time: 8:26 PM
 */
namespace helper;

class GenerateField
{
    /**
     * 根据选择的字段类型生成mysql需要的数据类型
     * @param $param
     * @return string
     */
    public static function parse($param)
    {
        $field = '';
        $fieldType = config('xcms.field');

        switch ($param['dtype']) {
            case 'text':
            case 'img':
            case 'int':
            case 'multitext':
            case 'switch':
                $field = $fieldType[$param['dtype']]['field'] . '(' . $fieldType[$param['dtype']]['len'] . ')';
                break;
            case 'checkbox':
            case 'radio':
            case 'select':
                $map = explode(',', $param['dfvalue']);
                $str = '';
                foreach ($map as $vo) {
                   $str .= "'" . $vo . "',";
                }
                $field = $fieldType[$param['dtype']]['field'] . '(' . rtrim($str, ',') . ')';
                break;
            case 'decimal':
            case 'float':
                $field = $fieldType[$param['dtype']]['field'] . '(' . $fieldType[$param['dtype']]['len'] . ',2)';
                break;
            case 'htmltext':
            case 'datetime':
            case 'imgs':
                $field = $fieldType[$param['dtype']]['field'];
                break;
        }

        return $field;
    }
}