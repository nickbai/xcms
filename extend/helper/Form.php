<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/30
 * Time: 5:32 PM
 */
namespace helper;

class Form
{
    /**
     * 构建表单
     * @param $label
     * @param $param
     * @return mixed
     */
    public static function build($label, $param)
    {
        $func = 'form' . ucfirst($label);
        return self::$func($param);
    }

    /**
     * 构建普通input输入框
     * @param $param
     * @return array
     */
    private static function formText($param)
    {
        $title = $param['title'];
        $name = $param['name'];

        $input =<<<EOL
<div class="layui-form-item">
    <label class="layui-form-label">{$title}</label>
    <div class="layui-input-block">
        <input type="text" name="fieldExt[{$name}]" autocomplete="off" class="layui-input">
    </div>
</div>
EOL;
        return [
            'html' => $input,
            'js' => ''
        ];
    }

    /**
     * 构建复选框
     * @param $param
     * @return array
     */
    private static function formCheckbox($param)
    {
        $title = $param['title'];
        $name = $param['name'];
        $value = explode(',', $param['value']);

        $checkbox =<<<EOL
<div class="layui-form-item">
    <label class="layui-form-label">{$title}</label>
    <div class="layui-input-block">
EOL;
        foreach($value as $key => $vo) {
            $checkbox .= '<input type="checkbox" name="fieldExt[' . $name . '][]" title="' . $vo . '" value="' . $vo . '">';
        }

        $checkbox .= ' </div>
  </div>';

        return [
            'html' => $checkbox,
            'js' => ''
        ];
    }

    /**
     * 构建复选框
     * @param $param
     * @return array
     */
    private static function formDatetime($param)
    {
        $title = $param['title'];
        $name = $param['name'];

        $datetime =<<<EOL
<div class="layui-form-item">
    <label class="layui-form-label">{$title}</label>
    <div class="layui-input-block">
        <input type="text" name="fieldExt[{$name}]" autocomplete="off" class="layui-input" id="idx-{$name}">
    </div>
</div>
EOL;
        $js =<<<EOL
layui.use('laydate', function() {
    
    var laydate = layui.laydate;
    laydate.render({
        elem: '#idx-{$name}'
    });
});
EOL;

        return [
            'html' => $datetime,
            'js' => $js
        ];
    }

    /**
     * 构建数字类型
     * @param $param
     * @return array
     */
    private static function formDecimal($param)
    {
        $title = $param['title'];
        $name = $param['name'];
        $unit = $param['unit'];

        if (empty($unit)) {

            $input =<<<EOL
<div class="layui-form-item">
    <label class="layui-form-label">{$title}</label>
    <div class="layui-input-block">
        <input type="text" name="fieldExt[{$name}]" value="0.00" autocomplete="off" class="layui-input" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^0-9\.]/g,''));">
    </div>
</div>
EOL;
        } else {

            $input =<<<EOL
<div class="layui-form-item">
    <label class="layui-form-label">{$title}</label>
    <div class="layui-input-inline">
        <input type="text" name="fieldExt[{$name}]" value="0.00" autocomplete="off" class="layui-input" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^0-9\.]/g,''));">
    </div>
    <div class="layui-form-mid layui-word-aux">{$unit}</div>
</div>
EOL;
        }

        return [
            'html' => $input,
            'js' => ''
        ];
    }

    /**
     * 构建小数类型
     * @param $param
     * @return array
     */
    private static function formFloat($param)
    {
        return self::formDecimal($param);
    }

    /**
     * 构建富文本
     * @param $param
     * @return array
     */
    private static function formHtmltext($param)
    {
        $title = $param['title'];
        $name = $param['name'];

        $input =<<<EOL
<div class="layui-form-item">
    <label class="layui-form-label">{$title}</label>
    <div class="layui-input-block">
        <script id="idx-{$name}" name="{$name}" type="text/plain"></script>
    </div>
</div>
EOL;
        $js =<<<EOL
UE.getEditor('idx-{$name}', {
    serverUrl :"/login.php/system.ueditor/index/savepath/ueditor.html",
    zIndex: 999,
    initialFrameWidth: "100%", // 初化宽度
    initialFrameHeight: 450, // 初化高度            
    focus: false, // 初始化时，是否让编辑器获得焦点true或false
    maximumWords: 99999,
    removeFormatAttributes: 'class,style,lang,width,height,align,hspace,valign',// 允许的最大字符数
    pasteplain:false, // 是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
    autoHeightEnabled: false,
    toolbars : [[
        "fullscreen", "source", "|", "undo", "redo", "|", "bold", "italic", "underline", "fontborder", "strikethrough",
        "superscript", "subscript", "removeformat", "formatmatch", "autotypeset", "blockquote", "pasteplain", "|",
        "forecolor", "backcolor", "insertorderedlist", "insertunorderedlist", "|", "rowspacingtop", "rowspacingbottom",
        "lineheight", "|", "customstyle", "paragraph", "fontfamily", "fontsize", "|", "directionalityltr", "directionalityrtl", 
        "indent", "|", "justifyleft", "justifycenter", "justifyright", "justifyjustify", "|", "touppercase", "tolowercase", 
        "|", "link", "unlink", "|", "imagenone", "imageleft", "imageright", "imagecenter",
        "|", "simpleupload", "insertimage", "emotion", "insertvideo", "attachment", "map", "insertframe", "insertcode",
        "|", "horizontal", "spechars", "|", "inserttable", "deletetable", "insertparagraphbeforetable", "insertrow", 
        "deleterow", "insertcol", "deletecol", "mergecells", "mergeright", "mergedown", "splittocells", "splittorows",
        "splittocols", "charts", "|", "preview", "searchreplace", "drafts"
    ]]
});
EOL;
        return [
            'html' => $input,
            'js' => $js
        ];

    }
}