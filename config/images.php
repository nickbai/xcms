<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/27
 * Time: 4:16 PM
 */
return [
    /**
     * 可上传的默认图片后缀
     */
    'ext'  => 'jpg|png|bmp|jpeg|gif',

    /**
     * 图片允许类型
     */
    'acceptMime' => 'image/jpg,image/png,image/bmp,image/jpeg,image/gif'
];