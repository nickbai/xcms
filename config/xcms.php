<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/9
 * Time: 9:55 AM
 */
return [

    // 加密盐
    'salt' => 'NickBai123!@#',

    // 默认封号时间 5分钟
    'close_time' => 5,

    // 默认失败次数
    'max_fail_times' => 5,

    // 菜单类型
    'menu_type' => [
        1 => '系统',
        2 => '内容',
        3 => '会员',
        4 => '插件'
    ],

    // 自定义字段
    'field' => [
        'text' => [
            'field' => 'varchar',
            'len' => 255,
            'title' => '单行文本'
        ],
        'checkbox' => [
            'field' => 'set',
            'len' => '',
            'title' => '多选项'
        ],
        'datetime' => [
            'field' => 'datetime',
            'len' => 0,
            'title' => '日期类型'
        ],
        'decimal' => [
            'field' => 'decimal',
            'len' => 10,
            'title' => '金额类型'
        ],
        'float' => [
            'field' => 'float',
            'len' => 9,
            'title' => '小数类型'
        ],
        'htmltext' => [
            'field' => 'longtext',
            'len' => 0,
            'title' => 'HTML文本'
        ],
        'img' => [
            'field' => 'varchar',
            'len' => 155,
            'title' => '单张图'
        ],
        'imgs' => [
            'field' => 'text',
            'len' => 0,
            'title' => '多张图'
        ],
        'int' => [
            'field' => 'int',
            'len' => 10,
            'title' => '整数类型'
        ],
        'multitext' => [
            'field' => 'varchar',
            'len' => 512,
            'title' => '多行文本'
        ],
        'radio' => [
            'field' => 'enum',
            'len' => 0,
            'title' => '单选项'
        ],
        'select' => [
            'field' => 'enum',
            'len' => 0,
            'title' => '下拉框'
        ],
        'switch' => [
            'field' => 'tinyint',
            'len' => 1,
            'title' => '开关'
        ]
    ],

    // 属性标签
    'flag' => [
        'h' => '头条',
        'c' => '推荐',
        'a' => '置顶',
        'p' => '图片'
    ],

    // 栏目类型
    'channel_type' => [
        'channel' => '频道封面',
        'list' => '列表栏目',
        'link' => '外部链接'
    ]
];