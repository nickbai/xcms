layui.extend({
    dtree: '/static/common/dtree/dtree'
}).use(['form', 'dtree', 'layer'], function() {
    var form = layui.form;
    var dtree = layui.dtree, layer = layui.layer;

    // 监听提交
    form.on('submit(subForm)', function(data) {

        $.post(optUrl, data.field, function (res) {
            if (res.code == 0) {
                layer.msg(res.msg);

                setTimeout(function () {
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    parent.renderTable();
                }, 800);
            } else {
                layer.msg(res.msg);
            }
        }, 'json');
        return false;
    });

    // 检测选择类型
    form.on('radio(type)', function(data){
        showCheckedForm(data.value);
    });

    // 检测选择模型
    form.on('select(model_id)', function(data){
        showCheckedTpl(data.value);
    });

    // 初始化树
    dtree.renderSelect({
        elem: "#selTree1",
        data: data,
        dataStyle: 'layStyle',
        selectInitVal: pid
    });

    // 默认显示已有的图片
    if (hasPic) {
        layui.use('laytpl', function () {

            var laytpl = layui.laytpl;
            var data = {url: hasPic};
            showUploadPic(data, laytpl);
        });
    }
});

$(function () {
    showCheckedForm($("input[name='type']:checked").val());
    showCheckedTpl($("#model_id").val());

    $("#name").bind("input propertychange", function(event){
        $.post(getPinyinUrl, {name: $("#name").val()}, function (res) {
            if (res.code == 0) {
                $("#diyname").val(res.data);
            }
        }, 'json');
    });
});

// 根据选中的类型，显示对应的输入表单
function showCheckedForm(channelType) {

    if ('channel' == channelType) {

        $("#channel-tpl-div").show();
        $("#outlink-div").hide();
        $("#list-tpl-div").hide();
    } else if ('list' == channelType) {

        $("#channel-tpl-div").hide();
        $("#outlink-div").hide();
        $("#list-tpl-div").show();
    } else if ('link' == channelType) {

        $("#channel-tpl-div").hide();
        $("#outlink-div").show();
        $("#list-tpl-div").hide();
    }
}

// 根据选择的模型，显示对应的模板
function showCheckedTpl(modelId) {

    $.each(channelTpl, function (k, v) {
        if (k == modelId) {
            $("#channel_tpl").val(v.channel_tpl);
            $("#list_tpl").val(v.list_tpl);
            $("#show_tpl").val(v.show_tpl);
            return;
        }
    });
}

// 图片上传
layui.use(['upload', 'layer', 'laytpl'], function(){
    var upload = layui.upload;
    var layer = layui.layer;
    var laytpl = layui.laytpl;

    var index = null;
    upload.render({
        elem: '#upload'
        ,accept: 'images'
        ,acceptMime: acceptMime
        ,exts: exts
        ,url: uploadUrl
        ,before: function () {
            index = layer.load(0, {shade: false});
        }
        ,done: function(res) {
            layer.close(index);
            if (res.code == 0) {
                layer.msg(res.msg);
                showUploadPic(res.data, laytpl);
            } else if (203 == res.code) {
                showUploadPic(res.data, laytpl);
            } else {
                layer.msg(res.msg);
            }
        }
        ,error: function() {
            layer.close(index);
            layer.msg('上传失败');
        }
    });
});

// 显示上传的图片
function showUploadPic(data, laytpl) {
    var getTpl = $("#img-tpl").html()
        ,view = document.getElementById('img-list');
    laytpl(getTpl).render(data, function(html){
        view.innerHTML = html;
    });

    $("#image").val(data.url);
    $("#img-list").show();
}

// 删除图片
function removeImg() {
    $("#image").val('');
    $("#img-list").html('').hide();
}

// 显示图片选择层
var imgDialog = null;
function showImgDialog() {

    imgDialog = layer.open({
        type: 2,
        title: '图片选择',
        shade: 0.5,
        area: ['85%', '90%'],
        content: showPicUrl + '?num=1'
    });
}

// 使用图片
function useCheckedPic(pics) {

    layui.use('laytpl', function () {
        var laytpl = layui.laytpl;
        var data = {url: pics[0]};

        showUploadPic(data, laytpl);

        layer.close(imgDialog);
    });
}
