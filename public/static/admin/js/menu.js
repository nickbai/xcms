// 设置整体高度
var screenHeight = window.screen.height;
$('.tpl-content-wrapper').css('min-height', parseInt(screenHeight - 240) + 'px');

var menuIdsMap = new Map();

layui.use('element', function() {
    var element = layui.element;

    $(window).resize(function () {
        frameWH();
        autoLeftNav();
    });

    // 点击多层菜单
    $(".sidebar-nav-sub-menu").click(function () {
        operateMenu(this, element, 2);
    });

    // 点击单菜单
    $(".sidebar-nav-single").click(function () {
        operateMenu(this, element, 1);
    });

    // 监厅tab关闭
    element.on('tabDelete(tab)', function (data) {
        menuIdsMap.delete($(this).parent().attr('lay-id'));
    });

    // tab切换
    element.on('tab(tab)', function (data) {

        console.log($(this).attr('lay-id'));
    });
});

frameWH();
autoLeftNav();

function frameWH() {
    // 获取body高度
    var h = $('.left-sidebar').height();
    //设置tab的高度，
    $(".layui-tab").css("height", h+"px");
}

$(function () {

    // 侧边菜单
    $('.sidebar-nav-sub-title').on('click', function() {
        $(this).siblings('.sidebar-nav-sub').slideToggle(80)
            .end()
            .find('.sidebar-nav-sub-ico').toggleClass('sidebar-nav-sub-ico-rotate');
    });
});

function autoLeftNav() {

    $('.tpl-header-switch-button').on('click', function () {
        if ($('.left-sidebar').is('.active')) {
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').removeClass('active');
            }
            $('.left-sidebar').removeClass('active');
        } else {

            $('.left-sidebar').addClass('active');
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').addClass('active');
            }
        }
    });

    if ($(window).width() < 1024) {
        $('.left-sidebar').addClass('active');
    } else {
        $('.left-sidebar').removeClass('active');
    }
}

function operateMenu(obj, element, mType) {

    var ckMenu = $(obj).find('a');
    var type = ckMenu.attr('data-type');
    var menuId = ckMenu.attr('data-id');
    var title = ckMenu.attr('data-title');
    var url = ckMenu.attr('data-url');

    // 处理选中样式
    if (mType == 1) {
        ckMenu.removeClass('active').addClass('active').parent().siblings().find('a').removeClass('active');
    } else if (mType == 2) {
        ckMenu.removeClass('sub-active').addClass('sub-active').parent('li').siblings().find('a').removeClass('sub-active');
    }

    // 处理是tab显示的菜单
    if (type == 'tab') {
        // 选中的菜单不是首页
        if (menuId != 'xcms-home') {

            if (!menuIdsMap.get(menuId)) {

                menuIdsMap.set(menuId, 1);
                element.tabAdd('tab', {
                    title: title
                    ,content: '<iframe id="' + menuId + '" height="100%" width="100%" frameborder="0" src="' + url + '"></iframe>'
                    ,id: menuId
                });

            } else {
                // 刷新该iframe的内容
                $('#' + menuId).attr('src', url);
            }
        }

        element.tabChange('tab', menuId);
    } else if (type == 'blank') {

        // 新页面打开
        window.open(url);
    }
}