$(function () {
    // tab切换
    $('.login-menu div').click(function () {
        $(this).removeClass('menu-active').addClass('menu-active').siblings().removeClass('menu-active');
        var index = $(this).index();
        if (index == 0) {
            $('.login-div').show();
            $('.reg-div').hide();
            $("#login-captcha").click();
        } else {
            $('.login-div').hide();
            $('.reg-div').show();
            $("#reg-captcha").click();
        }
    });

    // 注册
    $(".reg-btn").click(function () {

        var userEmail = $("#reg-user-email").val();
        if (userEmail == '' || !validateMail(userEmail)) {
            $.toast("请输入正确的邮箱", "text");
            return false;
        }

        var userName = $("#reg-user-name").val();
        if (userName == '') {
            $.toast("请输入昵称", "text");
            return false;
        }

        var userPwd = $("#reg-user-pwd").val();
        if (userPwd == '') {
            $.toast("请输入密码", "text");
            return false;
        }

        var captcha = $("#reg-user-captcha").val();
        if (captcha == '') {
            $.toast("请输入验证码", "text");
            return false;
        }


    });

    // 登录
});

// 验证码点击
function changeCaptcha(obj) {
    $(obj).attr('src', $(obj).attr('src') + '?t=' + Math.random());
}

// 校验邮箱
function validateMail(mail) {
    if (mail != "") {
        var strRegex = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;
        if (!strRegex.test(mail)) {
            return false;
        }
    }
    return true;
}