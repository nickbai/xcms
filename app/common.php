<?php
// 应用公共文件
use app\admin\model\Node as NodeModel;

/**
 * 生成菜单树
 * @param $data
 * @return array
 */
function makeMenuTree($data) {

    $res = [];
    $tree = [];

    // 整理数组
    foreach ($data as $key => $vo) {
        // 此处是Dtree的特殊字段
        // http://www.wisdomelon.com/DTreeHelper/
        $vo['checkArr'] = ['type' => 0, 'checked' => 0];

        $res[$vo['id']] = $vo;
        $res[$vo['id']]['children'] = [];
    }
    unset($data);

    // 查询子孙
    foreach ($res as $key => $vo) {
        if($vo['parentId'] != 0){
            $res[$vo['parentId']]['children'][] = &$res[$key];
        }
    }

    // 去除杂质
    foreach ($res as $key => $vo) {
        if($vo['parentId'] == 0){
            $tree[] = $vo;
        }
    }
    unset($res);

    return $tree;
}

/**
 * 获取全部的节点数据
 * @return array
 */
function getNodeData() {
    try {

        $nodeModel = new NodeModel();
        $nodeInfo = $nodeModel->getAllNode();

        if ($nodeInfo['code'] != 0) {
            return ['code' => -1 , 'data' => [], 'msg' => $nodeInfo['msg']];
        }

        $tree = makeMenuTree($nodeInfo['data']->toArray());

        return ['code' => 0 , 'data' => $tree, 'msg' => $nodeInfo['msg']];

    } catch (\Exception $e) {

        return ['code' => -2 , 'data' => [], 'msg' => $e->getMessage()];
    }
}

/**
 * 快速美化打印助手函数
 * @param $data
 */
function p($data) {

    echo "<pre>";
    print_r($data);
    die;
}

/**
 * 获取密码
 * @param $password
 * @param $salt
 * @return string
 */
function makePassword($password, $salt = '') {

    if (empty($salt)) {
        return md5($password . config('xcms.salt'));
    }

    return md5($password . $salt);
}

/**
 * 按钮权限判定
 * @param $node
 * @return bool
 */
function buttonAuth($node) {

    $authMap = json_decode(session('auth'), true);
    if (!isset($authMap[$node]) && session('admin_id') != 1) {
        return false;
    }

    return true;
}

/**
 * 生成子孙树
 * @param $data
 * @return array
 */
function makeTree($data) {

    $res = [];
    $tree = [];

    // 整理数组
    foreach ($data as $key => $vo) {
        if (!empty($vo['href'])) {
            $vo['href'] = url($vo['href'])->build();
        }
        $vo['target'] = '_self';
        $res[$vo['node_id']] = $vo;
        $res[$vo['node_id']]['child'] = [];
    }
    unset($data);

    // 查询子孙
    foreach ($res as $key => $vo) {
        if($vo['node_pid'] != 0){
            $res[$vo['node_pid']]['child'][] = &$res[$key];
        }
    }

    // 去除杂质
    foreach ($res as $key => $vo) {
        if($vo['node_pid'] == 0){
            $tree[] = $vo;
        }
    }
    unset($res);

    return $tree;
}

/**
 * 获取全拼标题
 * @param $title
 * @return string
 */
function getFullTitle($title) {

    $pinyinArr = (new Overtrue\Pinyin\Pinyin())->convert($title);

    $str = '';
    foreach ($pinyinArr as $vo) {
        $str .= $vo;
    }

    return $str;
}

/**
 * 获取简拼标题
 * @param $title
 * @return string
 */
function getFirstTitle($title) {

    return (new Overtrue\Pinyin\Pinyin())->abbr($title);
}

/**
 * 检测模板前缀是否合法
 * @param $fileName
 * @param $prefix
 * @return bool
 */
function checkTpl($fileName, $prefix) {

    $fileNameArr = explode('_', $fileName);
    if ($fileNameArr[0] != $prefix) {
        return false;
    }

    return true;
}

/**
 * 获取表前缀
 * @return mixed
 */
function getTablePrefix() {
    return config('database.connections.mysql.prefix');
}

/**
 * 校验模板的格式
 * @param $param
 * @return array
 */
function checkTplFormat($param) {

    if (isset($param['channel_tpl'])) {
        $channelTplRes = checkTpl($param['channel_tpl'], 'channel');
        if (!$channelTplRes) {
            return ['code' => -2, 'data' => [], 'msg' => '模型模板必须以channel_开头'];
        }
    }

    if (isset($param['list_tpl'])) {
        $listTplRes = checkTpl($param['list_tpl'], 'list');
        if (!$listTplRes) {
            return ['code' => -3, 'data' => [], 'msg' => '栏目模板必须以list_开头'];
        }
    }

    if (isset($param['show_tpl'])) {
        $showTplRes = checkTpl($param['show_tpl'], 'show');
        if (!$showTplRes) {
            return ['code' => -4, 'data' => [], 'msg' => '详情页模板必须以show_开头'];
        }
    }

    return ['code' => 0, 'data' => [], 'msg' => ''];
}
