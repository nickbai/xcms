<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/5
 * Time: 7:34 PM
 */
namespace app\admin\model;

use think\Model;

class Role extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    /**
     * 获取角色列表
     * @param $limit
     * @param $where
     * @return array
     */
    public function getRoleList($limit, $where)
    {
        try {

            $list = $this->where($where)->order('role_id desc')->paginate($limit);
            return ['code' => 0, 'data' => $list, 'msg' => 'ok'];
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }

    /**
     * 添加角色
     * @param $param
     * @return array
     */
    public function addRole($param)
    {
        try {

            $has = $this->where('role_name', $param['role_name'])->find();
            if (!empty($has)) {
                return ['code' => -2, 'data' => [], 'msg' => '该角色已经存在'];
            }

            $this->save($param);
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '添加成功'];
    }

    /**
     * 根据id获取角色信息
     * @param $roleId
     * @return array
     */
    public function findRoleById($roleId)
    {
        try {

            $info = $this->where('role_id', $roleId)->find();
            return ['code' => 0, 'data' => $info, 'msg' => 'ok'];
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑角色
     * @param $param
     * @return array
     */
    public function editRole($param)
    {
        try {

            $has = $this->where('role_name', $param['role_name'])->where('role_id', '<>', $param['role_id'])->find();
            if (!empty($has)) {
                return ['code' => -2, 'data' => [], 'msg' => '该角色已经存在'];
            }

            $this->where('role_id', $param['role_id'])->save($param);
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '编辑成功'];
    }

    /**
     * 删除角色
     * @param $roleId
     * @return array
     */
    public function delRole($roleId)
    {
        try {

            $this->where('role_id', $roleId)->delete();
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '删除成功'];
    }

    /**
     * 获取所有的角色
     * @return array
     */
    public function getAllRole()
    {
        try {

            $roleInfo = $this->field('role_id,role_name')->where('status', 1)->select();
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $roleInfo, 'msg' => '删除成功'];
    }
}