<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/11
 * Time: 6:21 PM
 */
namespace app\admin\model;

use think\facade\Db;
use think\Model;

class Modelx extends Model
{
    /**
     * 获取模型
     * @param $limit
     * @param $where
     * @return array
     */
    public function getModelList($limit, $where)
    {
        try {

            $list = $this->where($where)->order('model_id desc')->paginate($limit);
            return ['code' => 0, 'data' => $list, 'msg' => 'ok'];
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }

    /**
     * 添加模型
     * @param $param
     * @return array
     */
    public function addModel($param)
    {
        try {

            $has = $this->where('uuid', $param['uuid'])->find();
            if (!empty($has)) {
                return ['code' => -10, 'data' => [], 'msg' => '该模型标识已经存在'];
            }

            $modelId = $this->insertGetId($param);

            $prefix = getTablePrefix();
            $sql = "CREATE TABLE `{$prefix}{$param['table']}_content` (`id` int(11) NOT NULL,`aid` int(11) NOT NULL,`content` longtext NOT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='{$param['name']}表'";
            Db::execute($sql);

            // 记录该表的默认字段
            (new ModelField())->addField([
                'name' => 'content',
                'model_id' => $modelId,
                'title' => '内容',
                'dtype' => 'htmltext',
                'define' => 'longtext',
                'max_length' => 0,
                'dfvalue' => '',
                'dfvalue_unit' => '',
                'sort_order' => 100,
                'status' => 1,
                'add_time' => date('Y-m-d H:i:s')
            ]);

        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '添加成功'];
    }

    /**
     * 获取模型信息
     * @param $modelId
     * @return array
     */
    public function getModelById($modelId)
    {
        try {

            $info = $this->where('model_id', $modelId)->find();
            return ['code' => 0, 'data' => $info, 'msg' => 'ok'];
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑模型
     * @param $param
     * @return array
     */
    public function editModel($param)
    {
        try {

            $has = $this->where('uuid', $param['uuid'])->where('model_id', '<>', $param['model_id'])->find();
            if (!empty($has)) {
                return ['code' => -10, 'data' => [], 'msg' => '该模型标识已经存在'];
            }

            $this->where('model_id', $param['model_id'])->update($param);

        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '编辑成功'];
    }

    /**
     * 删除模型内容表
     * @param $flag
     * @return array
     */
    public function dropModelTable($flag)
    {
        try {

            Db::execute('DROP TABLE `' . getTablePrefix() . $flag . '_content`');
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '删除成功'];
    }

    /**
     * 删除模型
     * @param $modelId
     * @return array
     */
    public function delModel($modelId)
    {
        try {

            $this->where('model_id', $modelId)->delete();
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '删除成功'];
    }

    /**
     * 获取所有的模型
     * @return array
     */
    public function getAllModelx()
    {
        try {

            $info = $this->select();
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $info, 'msg' => 'success'];
    }
}