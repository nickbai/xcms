<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/9
 * Time: 9:58 AM
 */
namespace app\admin\model;

use think\Model;

class Admin extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    /**
     * 获取管理员列表
     * @param $limit
     * @param $where
     * @return array
     */
    public function getAdminList($limit, $where)
    {
        try {

            $list = $this->field('a.*,r.role_name')->alias('a')
                ->join('role r', 'r.role_id = a.role_id')
                ->where($where)
                ->order('admin_id desc')->paginate($limit);
            return ['code' => 0, 'data' => $list, 'msg' => 'ok'];
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }

    /**
     * 添加管理员
     * @param $param
     * @return array
     */
    public function addAdmin($param)
    {
        try {

            $has = $this->where('admin_name', $param['admin_name'])->find();
            if (!empty($has)) {
                return ['code' => -2, 'data' => [], 'msg' => '该管理员已经存在'];
            }

            $this->save($param);
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '添加成功'];
    }

    /**
     * 编辑管理员
     * @param $param
     * @return array
     */
    public function editAdmin($param)
    {
        try {

            $has = $this->where('admin_name', $param['admin_name'])->where('admin_id', '<>', $param['admin_id'])->find();
            if (!empty($has)) {
                return ['code' => -2, 'data' => [], 'msg' => '该管理员已经存在'];
            }

            $this->where('admin_id', $param['admin_id'])->update($param);
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '编辑成功'];
    }

    /**
     * 通过id获取管理员信息
     * @param $adminId
     * @return array
     */
    public function findAdminById($adminId)
    {
        try {

            $info = $this->where('admin_id', $adminId)->find();
            return ['code' => 0, 'data' => $info, 'msg' => 'ok'];
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }

    /**
     * 删除管理员
     * @param $adminId
     * @return array
     */
    public function delAdmin($adminId)
    {
        try {

            $this->where('admin_id', $adminId)->delete();
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '删除成功'];
    }

    /**
     * 根据姓名查找管理员
     * @param $name
     * @return array
     */
    public function findAdminByName($name)
    {
        try {

            $info = $this->where('admin_name', $name)->find();
            return ['code' => 0, 'data' => $info, 'msg' => 'ok'];
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }
}