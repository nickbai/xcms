<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/22
 * Time: 10:12 AM
 */
namespace app\admin\model;

use think\Model;

class Archives extends Model
{
    /**
     * 删除一个模型下的全部文章
     * @param $modelId
     * @return array
     */
    public function delArchivesByModelId($modelId)
    {
        try {
            $this->where('model_id', $modelId)->delete();
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '删除成功'];
    }

    /**
     * 隐藏删除栏目的文章
     * @param $channelId
     * @return array
     */
    public function hideArchivesByChannelId($channelId)
    {
        try {

            $this->where('channel_id', $channelId)->update([
                'status' => 4
            ]);
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '删除成功'];
    }
}