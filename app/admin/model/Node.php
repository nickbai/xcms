<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/6
 * Time: 5:23 PM
 */
namespace app\admin\model;

use think\Model;

class Node extends Model
{
    /**
     * 获取节点数据
     * @return array
     */
    public function getNodesList()
    {
        try {

            $list = $this->field('node_id as id,node_name as title,node_pid as parentId,node_path,node_icon,add_time,is_menu')
                ->select()->toArray();
        }catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $list, 'msg' => 'success'];
    }

    /**
     * 获取所有的菜单节点
     * @return array
     */
    public function getAllNode()
    {
        try {

            $info = $this->field('node_id as id,node_name as title,node_pid as parentId')->select();
            return ['code' => 0, 'data' => $info, 'msg' => 'success'];
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }

    /**
     * 获取所有的菜单节点
     * @param $ids
     * @return array
     */
    public function getMenuNode($ids = [])
    {
        try {

            $where = [];
            if (!empty($ids)) {
                $where[] = ['node_id', 'in', $ids];
            }

            $info = $this->field('node_id,node_pid,node_type,node_name title,node_icon icon,node_path href')
                    ->where('is_menu', 2)->where($where)->select();
            return ['code' => 0, 'data' => $info, 'msg' => 'success'];
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }
    }

    /**
     * 添加节点
     * @param $param
     * @return array
     */
    public function addNode($param)
    {
        try {

            // 检测唯一
            $has = $this->field('node_id')->where('node_name', $param['node_name'])
                ->where('node_path', $param['node_path'])->find();

            if (!empty($has)) {
                return ['code' => -1, 'data' => [], 'msg' => '该节点已经存在'];
            }

            $param['add_time'] = date('Y-m-d H:i:s');

            $this->insert($param);
        }catch (\Exception $e) {
            return ['code' => -2, 'data' => $e->getMessage(), 'msg' => '系统错误'];
        }

        return ['code' => 0, 'data' => [], 'msg' => '添加成功'];
    }

    /**
     * 编辑节点
     * @param $param
     * @return array
     */
    public function editNode($param)
    {
        try {

            // 检测唯一
            $has = $this->field('node_id')->where('node_name', $param['node_name'])
                ->where('node_path', $param['node_path'])->where('node_id', '<>', $param['node_id'])->find();

            if (!empty($has)) {
                return ['code' => -1, 'data' => [], 'msg' => '该节点已经存在'];
            }

            $this->where('node_id', $param['node_id'])->update($param);
        }catch (\Exception $e) {
            return ['code' => -2, 'data' => $e->getMessage(), 'msg' => '系统错误'];
        }

        return ['code' => 0, 'data' => [], 'msg' => '编辑成功'];
    }

    /**
     * 根据id 获取节点信息
     * @param $id
     * @return array
     */
    public function findNodeInfoById($id)
    {
        try {

            $res = $this->where('node_id', $id)->find();
        }catch (\Exception $e) {
            return ['code' => -1, 'data' => ['node_name' => ''], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $res, 'msg' => 'success'];
    }

    /**
     * 根据id 获取节点信息
     * @param $ids
     * @return array
     */
    public function findNodeInfoByIds($ids)
    {
        try {

            $res = $this->field('node_id,node_path')->whereIn('node_id', $ids)->select();
        }catch (\Exception $e) {
            return ['code' => -1, 'data' => ['node_name' => ''], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $res, 'msg' => 'success'];
    }

    /**
     * 根据id 删除节点
     * @param $id
     * @return array
     */
    public function deleteNodeById($id)
    {
        try {

            // 检测节点下是否有其他的节点
            $has = $this->where('node_pid', $id)->count();
            if ($has > 0) {
                return ['code' => -2, 'data' => [], 'msg' => '该节点下尚有其他节点，不可删除'];
            }

            $this->where('node_id', $id)->delete();

        }catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '删除节点成功'];
    }

    /**
     * 获取节点的id
     * @param $path
     * @return array
     */
    public function findNodeIdByPath($path)
    {
        try {

            $res = $this->field('node_id')->where('node_path', $path)->find();

        }catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $res, 'msg' => 'success'];
    }
}