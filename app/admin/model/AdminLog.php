<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/9
 * Time: 9:59 AM
 */
namespace app\admin\model;

use think\Model;

class AdminLog extends Model
{
    /**
     * 记录日志
     * @param $param
     * @return array
     */
    public function addAdminLog($param)
    {
        try {

            $param['create_time'] = date('Y-m-d H:i:s');
            $this->insert($param);
        } catch (\Exception $e) {
            return ['code' => -1, 'data' => [], 'msg' => '系通异常'];
        }

        return ['code' => 0, 'data' => [], 'msg' => 'ok'];
    }
}