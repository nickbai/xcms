<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/28
 * Time: 5:56 PM
 */
namespace app\admin\model;

use think\Model;

class Channel extends Model
{
    /**
     * 获取全部模型列表
     * @return array
     */
    public function getChannelList()
    {
        try {

            $list = $this->select()->toArray();
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $list, 'msg' => 'success'];
    }

    /**
     * 获取所有的栏目树
     * @return array
     */
    public function getAllChannelTree()
    {
        try {

            $list = $this->field('channel_id id,parent_id parentId,name title')->select()->toArray();
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $list, 'msg' => 'success'];
    }

    /**
     * 添加栏目
     * @param $param
     * @return array
     */
    public function addChannel($param)
    {
        try {

            $has = $this->field('channel_id')->where('name', $param['name'])->find();
            if (!empty($has)) {
                return ['code' => -1, 'data' => [], 'msg' => '该栏目名称已经存在'];
            }

            $has = $this->field('channel_id')->where('diyname', $param['diyname'])->find();
            if (!empty($has)) {
                return ['code' => -2, 'data' => [], 'msg' => '栏目自定义路由已经存在'];
            }

            $this->save($param);
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '添加成功'];
    }

    /**
     * 修改栏目状态
     * @param $param
     * @return array
     */
    public function updateChannelStatus($param)
    {
        try {

            $this->where('channel_id', $param['channel_id'])->update($param);
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '修改成功'];
    }

    /**
     * 根据id获取栏目的信息
     * @param $channelId
     * @return array
     */
    public function getChannelById($channelId)
    {
        try {

            $info = $this->where('channel_id', $channelId)->find();
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => $info, 'msg' => 'success'];
    }

    /**
     * 编辑栏目
     * @param $param
     * @return array
     */
    public function editChannel($param)
    {
        try {

            $has = $this->field('channel_id')->where('channel_id', '<>', $param['channel_id'])
                ->where('name', $param['name'])->find();
            if (!empty($has)) {
                return ['code' => -1, 'data' => [], 'msg' => '该栏目名称已经存在'];
            }

            $has = $this->field('channel_id')->where('channel_id', '<>', $param['channel_id'])
                ->where('diyname', $param['diyname'])->find();
            if (!empty($has)) {
                return ['code' => -2, 'data' => [], 'msg' => '栏目自定义路由已经存在'];
            }

            $this->where('channel_id', $param['channel_id'])->update($param);
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '编辑成功'];
    }

    /**
     * 根据id删除栏目
     * @param $channelId
     * @return array
     */
    public function delChannelById($channelId)
    {
        try {

            $hasChild = $this->field('channel_id')->where('parent_id', $channelId)->find();
            if (!empty($hasChild)) {
                return ['code' => -2, 'data' => [], 'msg' => '该栏目下有子栏目不可直接删除'];
            }

            $this->where('channel_id', $channelId)->delete();
            // 隐藏属于该栏目的文章
            $archivesModel = new Archives();
            $archivesModel->hideArchivesByChannelId($channelId);
        } catch (\Exception $e) {

            return ['code' => -1, 'data' => [], 'msg' => $e->getMessage()];
        }

        return ['code' => 0, 'data' => [], 'msg' => '删除成功'];
    }
}