<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/30
 * Time: 5:10 PM
 */
namespace app\admin\controller;

use think\facade\View;

class Article extends Base
{
    // 文档列表
    public function index()
    {
        if (request()->isAjax()) {

        }

        return View::fetch();
    }
}