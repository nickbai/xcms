<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/14
 * Time: 9:25 PM
 */
namespace app\admin\controller;

use app\admin\model\ModelField as ModelFieldModel;
use app\admin\validate\ModelFieldValidate;
use helper\GenerateField;
use think\exception\ValidateException;
use think\facade\View;

class ModelField extends Base
{
    // 展示自定义字段
    public function showField()
    {
        $modelId = input('param.model_id');

        if (request()->isAjax()) {

            if (empty($modelId)) {
                return json(['code' => 0, 'msg' => 'ok', 'count' => 0, 'data' => []]);
            }

            $where['model_id'] = $modelId;
            $limit = input('param.limit');

            $modelFieldModel = new ModelFieldModel();
            $list = $modelFieldModel->getModelFieldList($limit, $where);
            if(0 == $list['code']) {

                return json(['code' => 0, 'msg' => 'ok', 'count' => $list['data']->total(), 'data' => $list['data']->all()]);
            }

            return json(['code' => 0, 'msg' => 'ok', 'count' => 0, 'data' => []]);
        }

        View::assign([
            'mid' => $modelId
        ]);

        return View::fetch();
    }

    // 更新字段状态
    public function setFieldStatus()
    {
        if (request()->isAjax()) {

            $id = input('param.id');
            $status = input('param.status');

            $modelFieldModel = new ModelFieldModel();
            $res = $modelFieldModel->updateModelField($id, [
                'status' => $status
            ]);

            return json($res);
        }
    }

    // 增加内容字段
    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');
            $dtype = config('xcms.field');

            // 检验完整性
            try {

                validate(ModelFieldValidate::class)->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            $modelFieldModel = new ModelFieldModel();
            $res = $modelFieldModel->checkAndAddField($param, $dtype);

            return json($res);
        }

        View::assign([
            'dtype' => config('xcms.field'),
            'model_id' => input('param.model_id')
        ]);

        return View::fetch();
    }

    // 编辑内容字段
    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');
            $dtype = config('xcms.field');

            // 检验完整性
            try {

                validate(ModelFieldValidate::class)->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            $modelFieldModel = new ModelFieldModel();
            $res = $modelFieldModel->checkAndEditField($param, $dtype);

            return json($res);
        }

        $fieldId = input('param.id');
        $fieldModel = new ModelFieldModel();
        $fieldInfo = $fieldModel->getFieldInfoById($fieldId)['data'];

        View::assign([
            'dtype' => config('xcms.field'),
            'info' => $fieldInfo
        ]);

        return View::fetch();
    }

    // 删除字段
    public function del()
    {
        if (request()->isAjax()) {

            $id = input('param.id');
            $name = input('param.name');
            $modelId = input('param.model_id');

            $fieldModel = new ModelFieldModel();

            $res = $fieldModel->delField($id, $name, $modelId);
            return json($res);
        }
    }
}