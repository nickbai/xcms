<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/4
 * Time: 10:37 AM
 */
namespace app\admin\controller;

use app\admin\model\Role as RoleModel;
use app\admin\validate\RoleValidate;
use think\facade\View;
use think\exception\ValidateException;

class Role extends Base
{
    // 角色列表
    public function index()
    {
        if(request()->isAjax()) {

            $limit = input('param.limit');
            $roleName = input('param.role_name');

            $where = [];
            if (!empty($roleName)) {
                $where[] = ['role_name', 'like', $roleName . '%'];
            }

            $roleModel = new RoleModel();
            $list = $roleModel->getRoleList($limit, $where);

            if(0 == $list['code']) {

                return json(['code' => 0, 'msg' => 'ok', 'count' => $list['data']->total(), 'data' => $list['data']->all()]);
            }

            return json(['code' => 0, 'msg' => 'ok', 'count' => 0, 'data' => []]);
        }

        return View::fetch();
    }

    // 添加角色
    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            // 检验完整性
            try {

                validate(RoleValidate::class)->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            $roleModel = new RoleModel();
            $res = $roleModel->addRole($param);

            return json($res);
        }

        $nodeData = getNodeData();
        View::assign([
            'node' => json_encode($nodeData['data'])
        ]);

        return View::fetch();
    }

    // 编辑角色
    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            if ($param['role_id'] == 1) {
                return json(['code' => -2, 'data' => [], 'msg' => '超级管理员不可以操作']);
            }

            // 检验完整性
            try {

                validate(RoleValidate::class)->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            $param['update_time'] = date('Y-m-d H:i:s');

            $roleModel = new RoleModel();
            $res = $roleModel->editRole($param);

            return json($res);
        }

        $roleId = input('param.id');

        $roleModel = new RoleModel();
        $roleInfo = $roleModel->findRoleById($roleId);
        $nodeData = getNodeData();

        View::assign([
            'role' => $roleInfo['data'],
            'node' => json_encode($nodeData['data'])
        ]);

        return View::fetch();
    }

    // 删除角色
    public function del()
    {
        $roleId = input('param.id');

        if ($roleId == 1) {
            return json(['code' => -2, 'data' => [], 'msg' => '超级管理员不可以操作']);
        }

        $roleModel = new RoleModel();
        $res = $roleModel->delRole($roleId);

        return json($res);
    }
}