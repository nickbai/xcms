<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/10
 * Time: 8:59 PM
 */
namespace app\admin\controller;

use app\BaseController;
use helper\Form;
use think\facade\View;

class Base extends BaseController
{
    // 初始化
    protected function initialize()
    {
        $html = Form::build('htmltext', [
            'title' => '性别',
            'name' => 'sex',
            'value' => '男,女,保密',
            'unit' => ''
        ]);
        p($html);
        if (empty(session('admin_name'))) {
            header('location:' . url('login/index'));
            exit;
        }

        $controller = strtolower(request()->controller());
        $action = strtolower(request()->action());

        $checkAuth = $controller . '/' . $action;
        $authMap = json_decode(session('auth'), true);
        if (!isset($authMap[$checkAuth]) && session('admin_id') != 1) {

            if (request()->isAjax()) {
                exit(json_encode(['code' => 403, 'data' => [], 'msg' => '您没有权限']));
            }

            exit(file_get_contents(app()->getAppPath() . 'view/error_tpl.html'));
        }

        View::assign([
            'admin_name' => session('admin_name'),
            'admin_id' => session('admin_id'),
            'nickname' => session('nickname')
        ]);
    }
}