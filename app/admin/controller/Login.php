<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/10
 * Time: 9:14 PM
 */
namespace app\admin\controller;

use app\admin\model\AdminLog;
use app\admin\validate\LoginValidate;
use think\exception\ValidateException;
use think\facade\View;

class Login
{
    public function index()
    {
        return View::fetch();
    }

    public function doLogin()
    {
        if (request()->isPost()) {

            $param = input('post.');

            // 检验完整性
            try {

                validate(LoginValidate::class)->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            // 验证码校验
            if (!captcha_check($param['verifyCode'])) {
                return json(['code' => -6, 'data' => [], 'msg' => '验证码不正确']);
            }

            $adminModel = new \app\admin\model\Admin();
            $info = $adminModel->findAdminByName($param['admin_name']);

            if (0 != $info['code']) {
                return json(['code' => -2, 'data' => [], 'msg' => '系统错误']);
            }

            $adminInfo = $info['data'];
            if (empty($adminInfo)) {
                return json(['code' => -3, 'data' => [], 'msg' => '管理员不存在']);
            }

            $adminLogModel = new AdminLog();
            if ($adminInfo['status'] == 0) {
                // 记录错误日志
                $adminLogModel->addAdminLog([
                    'admin_id' => $adminInfo['admin_id'],
                    'admin_name' => $adminInfo['admin_name'],
                    'operate_url' => 'login/doLogin',
                    'log_title' => '管理员登录',
                    'content' => '该账号已被封禁',
                    'operator_ip' => request()->ip(),
                    'user_agent' => request()->header()['user-agent']
                ]);

                return json(['code' => -4, 'data' => [], 'msg' => '该账号已被封禁']);
            }

            if ($adminInfo['status'] == 2) {

                $diff = strtotime($adminInfo['last_login_time']) - time();
                if ($diff > 0) {

                    $adminLogModel->addAdminLog([
                        'admin_id' => $adminInfo['admin_id'],
                        'admin_name' => $adminInfo['admin_name'],
                        'operate_url' => 'login/doLogin',
                        'log_title' => '管理员登录',
                        'content' => '登录失败次数过多账号被查封',
                        'operator_ip' => request()->ip(),
                        'user_agent' => request()->header()['user-agent']
                    ]);

                    return json(['code' => -5, 'data' => [], 'msg' => '尝试过多，请' . ceil($diff / 60) . '分钟后再试']);
                }
            }

            $checkPwd = makePassword($param['admin_password'], $adminInfo['salt']);
            if ($checkPwd != $adminInfo['admin_password']) {
                // 记录错误日志 累计错误次数
                $adminLogModel->addAdminLog([
                    'admin_id' => $adminInfo['admin_id'],
                    'admin_name' => $adminInfo['admin_name'],
                    'operate_url' => 'login/doLogin',
                    'log_title' => '管理员登录',
                    'content' => '密码错误登录失败',
                    'operator_ip' => request()->ip(),
                    'user_agent' => request()->header()['user-agent']
                ]);

                $failTimes = $adminInfo['fail_times'] + 1;
                $maxFailTimes = config('xcms.max_fail_times');
                $status = 1;
                $msg = '用户名密码错误,您还有' . ($maxFailTimes - $failTimes) . '次尝试机会';
                $time = config('xcms.close_time');

                if ($failTimes >= $maxFailTimes) {
                    $status = 2;
                    $msg = '尝试过多,请' . $time . '分钟后再试';
                }

                $adminModel->editAdmin([
                    'admin_id' => $adminInfo['admin_id'],
                    'admin_name' => $adminInfo['admin_name'],
                    'fail_times' => $failTimes,
                    'status' => $status,
                    'last_login_time' => date('Y-m-d H:i:s', time() + $time * 60)
                ]);

                return json(['code' => -5, 'data' => [], 'msg' => $msg]);
            }

            $adminModel->editAdmin([
                'admin_id' => $adminInfo['admin_id'],
                'admin_name' => $adminInfo['admin_name'],
                'fail_times' => 0,
                'status' => 1,
                'last_login_time' => date('Y-m-d H:i:s'),
                'last_login_ip' => request()->ip()
            ]);

            // 角色信息
            $roleModel = new \app\admin\model\Role();
            $roleInfo = $roleModel->findRoleById($adminInfo['role_id'])['data'];
            if (empty($roleInfo)) {
                return json(['code' => -6, 'data' => [], 'msg' => '角色信息不完整']);
            }

            // admin管理员跳过
            $nodesMap = [];
            if (1 != $adminInfo['admin_id']) {
                $nodesModel = new \app\admin\model\Node();
                $nodesInfo = $nodesModel->findNodeInfoByIds($roleInfo['role_node']);

                foreach ($nodesInfo['data'] as $vo) {
                    if (empty($vo['node_path'])) {
                        $nodesMap[$vo['node_id'] . '_top'] = $vo['node_id'];
                    } else {
                        $nodesMap[$vo['node_path']] = $vo['node_id'];
                    }
                }
            }

            session('admin_name', $adminInfo['admin_name']);
            session('admin_id', $adminInfo['admin_id']);
            session('nickname', $adminInfo['admin_nickname']);
            session('role_id', $adminInfo['role_id']);
            session('auth', json_encode($nodesMap));

            return json(['code' => 0, 'data' => [], 'msg' => '登录成功']);
        }
    }

    public function loginOut()
    {
        session('admin_name', null);
        session('admin_id', null);
        session('nickname', null);
        session('role_id', null);
        session('auth', null);

        return redirect('/' . array_keys(config('app.app_map'))['0'] . '.php');
    }
}