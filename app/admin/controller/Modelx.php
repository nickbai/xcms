<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/11
 * Time: 6:07 PM
 */
namespace app\admin\controller;

use app\admin\model\Archives;
use app\admin\validate\ModelxValidate;
use think\exception\ValidateException;
use think\facade\View;

class Modelx extends Base
{
    // 模型列表
    public function index()
    {
        if(request()->isAjax()) {

            $limit = input('param.limit');
            $modelName = input('param.model_name');

            $where = [];
            if (!empty($modelName)) {
                $where[] = ['name', 'like', $modelName . '%'];
            }

            $modelxModel = new \app\admin\model\Modelx();
            $list = $modelxModel->getModelList($limit, $where);

            if(0 == $list['code']) {

                return json(['code' => 0, 'msg' => 'ok', 'count' => $list['data']->total(), 'data' => $list['data']->all()]);
            }

            return json(['code' => 0, 'msg' => 'ok', 'count' => 0, 'data' => []]);
        }

        return View::fetch();
    }

    // 添加模型
    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');
            $param['table'] = $param['uuid'];

            // 检验完整性
            try {

                validate(ModelxValidate::class)->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            // 校验模板的完整性
            $res = checkTplFormat($param);
            if ($res['code'] != 0) {
                return json($res);
            }

            $modelxModel = new \app\admin\model\Modelx();
            $res = $modelxModel->addModel($param);

            return json($res);
        }

        return View::fetch();
    }

    // 编辑模型
    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');
            $param['table'] = $param['uuid'];

            // 检验完整性
            try {

                validate(ModelxValidate::class)->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            // 校验模板的完整性
            $res = checkTplFormat($param);
            if ($res['code'] != 0) {
                return json($res);
            }

            $modelxModel = new \app\admin\model\Modelx();
            $res = $modelxModel->editModel($param);

            return json($res);
        }

        $modelId = input('param.id');
        $modelxModel = new \app\admin\model\Modelx();
        $info = $modelxModel->getModelById($modelId)['data'];

        View::assign([
            'info' => $info
        ]);

        return View::fetch();
    }

    // 删除模型
    public function del()
    {
        if (request()->isAjax()) {

            $modelId = input('param.id');

            $modelxModel = new \app\admin\model\Modelx();
            $modelInfo = $modelxModel->getModelById($modelId)['data'];
            if (empty($modelInfo)) {
                return json(['code' => -1, 'data' => [], 'msg' => '该模型不存在']);
            }

            // 删除内容表
            $modelxModel->dropModelTable($modelInfo['table']);
            // 删除模型
            $modelxModel->delModel($modelId);
            // 删除文章基础表
            $res = (new Archives())->delArchivesByModelId($modelId);

            return json($res);
        }
    }

    // 获取拼音标识
    public function getPinyin()
    {
        if (request()->isAjax()) {

            $title = input('param.name');
            if (empty($title)) {
                return json(['code' => -1, 'data' => '', 'msg' => 'success']);
            }

            $str = getFullTitle($title);

            return json(['code' => 0, 'data' => $str, 'msg' => 'success']);
        }
    }
}