<?php
/**
 * Created by PhpStorm.
 * Date: 2020/12/29
 * Time: 9:16 PM
 */
namespace app\admin\controller;

use think\facade\View;

class System extends Base
{
    // 配置列表
    public function index()
    {
        return View::fetch();
    }
}