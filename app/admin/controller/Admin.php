<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/3
 * Time: 5:54 PM
 */
namespace app\admin\controller;

use app\admin\model\Admin as AdminModel;
use app\admin\model\Role as RoleModel;
use app\admin\validate\AdminValidate;
use think\exception\ValidateException;
use think\facade\View;

class Admin extends Base
{
    // 管理员列表
    public function index()
    {
        if(request()->isAjax()) {

            $limit = input('param.limit');
            $adminName = input('param.admin_name');

            $where = [];
            if (!empty($adminName)) {
                $where[] = ['admin_name', 'like', $adminName . '%'];
            }

            $adminModel = new AdminModel();
            $list = $adminModel->getAdminList($limit, $where);

            if(0 == $list['code']) {

                return json(['code' => 0, 'msg' => 'ok', 'count' => $list['data']->total(), 'data' => $list['data']->all()]);
            }

            return json(['code' => 0, 'msg' => 'ok', 'count' => 0, 'data' => []]);
        }

        return View::fetch();
    }

    // 添加管理员
    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            // 检验完整性
            try {

                validate(AdminValidate::class)->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            $param['salt'] = config('xcms.salt');
            $param['admin_password'] = makePassword($param['admin_password']);

            $adminModel = new AdminModel();
            $res = $adminModel->addAdmin($param);

            return json($res);
        }

        $roleModel = new RoleModel();
        $roleInfo = $roleModel->getAllRole();
        View::assign([
            'role' => $roleInfo['data']
        ]);

        return View::fetch();
    }

    // 编辑管理员
    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            // 检验完整性
            try {

                validate(AdminValidate::class)->scene('edit')->check($param);
            } catch (ValidateException $e) {

                return json(['code' => -1, 'data' => [], 'msg' => $e->getError()]);
            }

            $param['update_time'] = date('Y-m-d H:i:s');

            if (!empty($param['admin_password'])) {
                $param['admin_password'] = makePassword($param['admin_password']);
            } else {
                unset($param['admin_password']);
            }

            $adminModel = new AdminModel();
            $res = $adminModel->editAdmin($param);

            return json($res);
        }

        $adminId = input('param.id');
        $adminModel = new AdminModel();
        $adminInfo = $adminModel->findAdminById($adminId);

        $roleModel = new RoleModel();
        $roleInfo = $roleModel->getAllRole();
        View::assign([
            'role' => $roleInfo['data'],
            'admin' => $adminInfo['data']
        ]);

        return View::fetch();
    }

    // 删除管理员
    public function del()
    {
        $adminId = input('param.id');

        if ($adminId == 1) {
            return json(['code' => -2, 'data' => [], 'msg' => '超级管理员不可以操作']);
        }

        $adminModel = new AdminModel();
        $res = $adminModel->delAdmin($adminId);

        return json($res);
    }
}