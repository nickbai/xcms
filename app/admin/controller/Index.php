<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/1
 * Time: 10:13 PM
 */
namespace app\admin\controller;

use think\facade\View;

class Index extends Base
{
    /**
     * 默认布局页面
     * @return string
     */
    public function index()
    {
        return View::fetch('/index');
    }

    /**
     * 默认首页
     */
    public function home()
    {
        return View::fetch('home');
    }

    public function init()
    {
        $menuModel = new \app\admin\model\Node();

        if (session('admin_id') == 1) {
            $menuInfo = $menuModel->getMenuNode()['data'];
        } else {
            $menuInfo = $menuModel->getMenuNode(array_values(json_decode(session('auth'), true)))['data'];
        }

        $menuType = config('xcms.menu_type');
        $menu = [
            "homeInfo" => [
                "title" => "首页",
                "href" => url('index/home')->build()
            ],
            "logoInfo" => [
                "title" => "XCMS",
                "image" => "/static/admin/images/logo.png"
            ],
            "menuInfo" => [

            ]
        ];

        $menuByType = [];
        $menuInfo = $menuInfo->toArray();
        foreach ($menuInfo as $key => $vo) {
            $menuByType[$vo['node_type']][] = $vo;
        }

        $finalMenu = [];
        foreach ($menuByType as $key => $vo) {

            $finalMenu[] = [
                'title' => $menuType[$key],
                'href' => '',
                'target' => '_self',
                'child' => makeTree($vo)
            ];
        }

        $menu['menuInfo'] = $finalMenu;
        return json($menu);
    }
}