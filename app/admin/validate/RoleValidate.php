<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/7
 * Time: 9:50 AM
 */
namespace app\admin\validate;

use think\Validate;

class RoleValidate extends Validate
{
    protected $rule =   [
        'role_name'  => 'require|max:25',
        'status'   => 'require',
        'role_node' => 'require'
    ];

    protected $message  =   [
        'role_name.require' => '名称不能为空',
        'role_name.max' => '角色名不能超过25个字符',
        'status.require' => '状态不能为空',
        'role_node.require'  => '请至少分配主页权限',
    ];
}