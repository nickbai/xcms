<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/28
 * Time: 6:29 PM
 */
namespace app\admin\validate;

use think\Validate;

class ChannelValidate extends Validate
{
    protected $rule =   [
        'type'  => 'require',
        'model_id'  => 'require',
        'name' => 'require',
        'diyname' => 'require'
    ];

    protected $message  =   [
        'type.require' => '类型不能为空',
        'model_id.require' => '模型不能为空',
        'name.require' => '名称不能为空',
        'diyname.require' => '自定义url不能为空'
    ];
}