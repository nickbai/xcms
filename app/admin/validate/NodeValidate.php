<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/7
 * Time: 9:50 AM
 */
namespace app\admin\validate;

use think\Validate;

class NodeValidate extends Validate
{
    protected $rule =   [
        'node_name'  => 'require|max:25',
        'is_menu' => 'require'
    ];

    protected $message  =   [
        'node_name.require' => '名称不能为空',
        'node_name.max' => '名成不能超过25个字符',
        'is_menu.require'  => '是否是菜单不能为空',
    ];
}