<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/9
 * Time: 12:09 PM
 */
namespace app\admin\validate;

use think\Validate;

class AdminValidate extends Validate
{
    protected $rule =   [
        'admin_name'  => 'require|max:25',
        'admin_nickname'  => 'require|max:25',
        'status'   => 'require',
        'role_id' => 'require',
        'admin_password' => 'require'
    ];

    protected $message  =   [
        'admin_name.require' => '名称不能为空',
        'admin_name.max' => '管理员名不能超过25个字符',
        'admin_nickname.require' => '昵称不能为空',
        'admin_nickname.max' => '管理员昵称不能超过25个字符',
        'status.require' => '状态不能为空',
        'role_id.require'  => '请选择角色',
        'admin_password.require'  => '请设置密码'
    ];

    protected $scene = [
        'edit'  =>  ['admin_name', 'admin_nickname', 'status', 'role_id'],
    ];
}