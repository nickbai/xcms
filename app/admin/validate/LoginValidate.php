<?php
/**
 * Created by PhpStorm.
 * Date: 2020/11/9
 * Time: 12:09 PM
 */
namespace app\admin\validate;

use think\Validate;

class LoginValidate extends Validate
{
    protected $rule =   [
        'admin_name'  => 'require',
        'verifyCode' => 'require',
        'admin_password' => 'require'
    ];

    protected $message  =   [
        'admin_name.require' => '名称不能为空',
        'verifyCode.require' => '验证码不能为空',
        'admin_password.require'  => '密码不能为空'
    ];
}