<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/14
 * Time: 9:34 PM
 */
namespace app\admin\validate;

use think\Validate;

class ModelFieldValidate extends Validate
{
    protected $rule =   [
        'name'  => 'require',
        'title'  => 'require'
    ];

    protected $message  =   [
        'name.require' => '字段名称不能为空',
        'title.require' => '字段标题不能为空'
    ];
}