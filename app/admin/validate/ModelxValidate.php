<?php
/**
 * Created by PhpStorm.
 * Date: 2021/1/11
 * Time: 6:22 PM
 */
namespace app\admin\validate;

use think\Validate;

class ModelxValidate extends Validate
{
    protected $rule =   [
        'name'  => 'require|max:25',
        'table'  => 'require|max:40',
        'uuid' => 'require|max:40'
    ];

    protected $message  =   [
        'name.require' => '名称不能为空',
        'name.max' => '名成不能超过25个字符',
        'table.require'  => '表名不能为空',
        'table.max'  => '表名不能超过40个字符',
        'uuid.require'  => '模型标识不能为空',
        'uuid.max'  => '模型标识不能超过40个字符',
    ];
}