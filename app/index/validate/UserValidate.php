<?php
/**
 * Created by PhpStorm.
 * Date: 2020/12/8
 * Time: 9:40 PM
 */
namespace app\index\validate;

use think\Validate;

class UserValidate extends Validate
{
    protected $rule =   [
        'user_email'  => 'require|email',
        'user_pwd' => 'require',
        'user_name' => 'require'
    ];

    protected $message  =   [
        'user_email.require' => '登录账号不能为空',
        'user_email.email' => '登录账号不是邮箱',
        'user_pwd.require'  => '密码不能为空',
        'user_name.require' => '昵称不能为空'
    ];
}