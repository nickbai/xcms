<?php
/**
 * Created by PhpStorm.
 * Date: 2020/12/8
 * Time: 9:31 PM
 */
namespace app\index\model;

use think\Model;

class User extends Model
{
    /**
     * 添加用户
     * @param $param
     * @return array
     */
    public function addUser($param)
    {
        try {

            $has = $this->where('user_email', $param['user_email'])->find();
            if (!empty($has)) {
                return ['code' => -1, 'data' => [], 'msg' => '该登录账号已经存在'];
            }

            $this->insert($param);

        } catch (\Exception $e) {
            return ['code' => -2, 'data' => $e->getMessage(), 'msg' => '系统错误'];
        }

        return ['code' => 0, 'data' => [], 'msg' => '注册成功'];
    }

    /**
     * 根据邮箱账号获取用户信息
     * @param $email
     * @return array
     */
    public function findUserByEmail($email)
    {
        try {

            $info = $this->where('user_email', $email)->find();
        } catch (\Exception $e) {
            return ['code' => -2, 'data' => $e->getMessage(), 'msg' => '系统错误'];
        }

        return ['code' => 0, 'data' => $info, 'msg' => 'success'];
    }
}