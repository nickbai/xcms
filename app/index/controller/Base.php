<?php
/**
 * Created by PhpStorm.
 * Date: 2020/12/8
 * Time: 7:08 PM
 */
namespace app\index\controller;

use app\BaseController;

class Base extends BaseController
{
    // 初始化
    protected function initialize()
    {
        if (empty(session('user_name'))) {
            header('location:' . url('login/index'));
            exit;
        }
    }
}